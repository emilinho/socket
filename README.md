# Programación con sockets en GNU/Linux

+ <https://github.com/zappala/socket-programming-examples-c>
+ <https://www.tutorialspoint.com/unix_sockets/index.htm>
+ <https://www.thegeekstuff.com/2011/12/c-socket-programming>
+ <https://www.binarytides.com/socket-programming-c-linux-tutorial/>
+ <https://www.binarytides.com/server-client-example-c-sockets-linux/>
+ <http://www.linuxhowtos.org/C_C++/socket.htm>
+ <https://en.wikibooks.org/wiki/C_Programming/Networking_in_UNIX>
+ <https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_72/rzab6/rzab6pdf.pdf>
+ <http://www.cs.miami.edu/home/schulz/CSC322.pdf>

